package gograndma

import (
	"github.com/rotisserie/eris"
)

type Runner interface {
	Run(dependency *Dependency) error
}

// This is a basic runner that will check if a Dependency is met, run its Meet() 
// function if not, and re-check if it is met. If it is still not  met after executing
// the Meet() function, the dependency will be marked as failed.
type DefaultRunner struct {
	meetOrder []string
}

// Create a new `DefaultRunner{}` 
func NewDefaultRunner() DefaultRunner {
	return DefaultRunner{
		meetOrder: []string{},
	}
}

// Execute a dependency. Check to see if any its dependencies are unment and 
// meet them if they are not. The dependency tree is walked with depth-first search.
func (r *DefaultRunner) Run(dependency *Dependency) error {
	for _, dep := range dependency.Dependencies() {
		if dep == nil {
			continue
		}

		if err := r.Run(dep); err != nil {
			return eris.Wrapf(
				err,
				"Failed to meet %s because of failed dependency %s",
				dependency.Name(),
				dep.Name(),
			)
		}
	}

	if met, err := dependency.Met(); err != nil {
		return eris.Wrapf(err, "Failed to check if %s has been met", dependency.Name())
	} else if met {
		return nil
	}

	var err error
	if err = dependency.Meet(); err == nil {
		if met, err := dependency.Met(); err != nil {
			return eris.Wrapf(err, "Failed to check if %s has been met", dependency.Name())
		} else if !met {
			return eris.Errorf("Failed to meet %s", dependency.Name())
		}

		r.meetOrder = append(r.meetOrder, dependency.Name())
		return nil
	}

	return err
}
