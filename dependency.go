package gograndma

type Dependency struct {
	name         string
	dependencies []*Dependency
	met          func() (bool, error)
	meet         func() error
}

func NewDependency(name string) Dependency {
	return Dependency{
		name:         name,
		dependencies: []*Dependency{},
		met: func() (bool, error) {
			return false, nil
		},
		meet: func() error {
			return nil
		},
	}
}

func (d Dependency) OnMet(met func() (bool, error)) Dependency {
	d.met = met
	return d
}

func (d Dependency) OnMeet(meet func() error) Dependency {
	d.meet = meet
	return d
}

func (d Dependency) HasDependency(dependency *Dependency) Dependency {
	d.dependencies = append(d.dependencies, dependency)
	return d
}

func (d *Dependency) Name() string {
	return d.name
}

func (d *Dependency) Dependencies() []*Dependency {
	return d.dependencies
}

func (d *Dependency) Met() (bool, error) {
	return d.met()
}

func (d *Dependency) Meet() error {
	return d.meet()
}
