package gograndma

import (
	"errors"
	"fmt"
	"reflect"
	"testing"
)

var depTestName = "some-name"
var depTestErr = errors.New("This is a test error")

func Test_Dependency_NameSet(t *testing.T) {
	dep := NewDependency(depTestName)

	if dep.Name() != depTestName {
		t.Fatalf("Name mismatch %s != %s", dep.Name(), depTestName)
	}
}

func Test_Dependency_OnMetSet(t *testing.T) {
	testMetFunc := func() (bool, error) {
		return true, depTestErr
	}

	dep := NewDependency(depTestName).
		OnMet(testMetFunc)

	output, err := dep.Met()

	if !output || !errors.Is(err, depTestErr) {
		t.Fatalf("output=%t err=%s", output, err)
	}
}

func Test_Dependency_OnMeetSet(t *testing.T) {
	testMeetFunc := func() error {
		return depTestErr
	}

	dep := NewDependency(depTestName).
		OnMeet(testMeetFunc)

	if err := dep.Meet(); !errors.Is(err, depTestErr) {
		t.Fatalf("err=%s", err)
	}
}

func Test_Dependency_DependenciesSet(t *testing.T) {
	dep1 := NewDependency(fmt.Sprintf("%s-1", depTestName))
	dep2 := NewDependency(fmt.Sprintf("%s-2", depTestName))
	dep3 := NewDependency(fmt.Sprintf("%s-3", depTestName))
	dep4 := NewDependency(fmt.Sprintf("%s-4", depTestName))
	dep5 := NewDependency(fmt.Sprintf("%s-5", depTestName))

	if !reflect.DeepEqual(dep1.Dependencies(), []*Dependency{}) {
		t.Fatalf("First dependency check failed %v", dep1.Dependencies())
	}

	dep1 = dep1.HasDependency(&dep3)
	if !reflect.DeepEqual(dep1.Dependencies(), []*Dependency{&dep3}) {
		t.Fatalf("Second dependency check failed %v", dep1.Dependencies())
	}

	dep1 = dep1.HasDependency(&dep2)
	if !reflect.DeepEqual(dep1.Dependencies(), []*Dependency{&dep3, &dep2}) {
		t.Fatalf("Third dependency check failed %v", dep1.Dependencies())
	}

	dep1 = dep1.HasDependency(&dep5)
	if !reflect.DeepEqual(dep1.Dependencies(), []*Dependency{&dep3, &dep2, &dep5}) {
		t.Fatalf("Fourth dependency check failed %v", dep1.Dependencies())
	}

	dep1 = dep1.HasDependency(&dep4)
	if !reflect.DeepEqual(dep1.Dependencies(), []*Dependency{&dep3, &dep2, &dep5, &dep4}) {
		t.Fatalf("Fifth dependency check failed %v", dep1.Dependencies())
	}
}
