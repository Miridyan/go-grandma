package gograndma

import (
	"testing"
)

// helper function to create base run order test dependencies
func newRunOrderTestDep(name string, metMap *map[string]bool, t *testing.T) Dependency {
	return NewDependency(name).
		OnMet(func() (bool, error) {
			return (*metMap)[name], nil
		}).
		OnMeet(func() error {
			if (*metMap)[name] {
				t.Fatalf("%s already met, meet should not run", name)
			}

			(*metMap)[name] = true
			return nil
		})
}

func Test_Runner_MeetOrder_NoDuplicates(t *testing.T) {
	metMap := make(map[string]bool, 0)

	firstDependency := newRunOrderTestDep("first-dep", &metMap, t)
	secondDependency := newRunOrderTestDep("second-dep", &metMap, t)
	thirdDependency := newRunOrderTestDep("third-dep", &metMap, t).
		HasDependency(&firstDependency).
		HasDependency(&secondDependency)

	fourthDependency := newRunOrderTestDep("fourth-dep", &metMap, t)
	fifthDependency := newRunOrderTestDep("fifth-dep", &metMap, t).
		HasDependency(&thirdDependency).
		HasDependency(&fourthDependency)

	runner := NewDefaultRunner()
	expectedRunOrder := []string{"first-dep", "second-dep", "third-dep", "fourth-dep", "fifth-dep"}

	if err := runner.Run(&fifthDependency); err != nil {
		t.Fatalf("Expected nil runner error, received %s", err)
	}

	if len(runner.meetOrder) != len(expectedRunOrder) {
		t.Fatalf(
			"Expected vs Actual run order length mismatch %d != %d",
			len(expectedRunOrder),
			len(runner.meetOrder),
		)
	}

	for i, expected := range expectedRunOrder {
		if expected != runner.meetOrder[i] {
			t.Fatalf("Run order mismatch at %d, %s != %s", i, expected, runner.meetOrder[i])
		}
	}
}

func Test_Runner_MeetOrder_Duplicates(t *testing.T) {
	metMap := make(map[string]bool, 0)

	firstDependency := newRunOrderTestDep("first-dep", &metMap, t)
	secondDependency := newRunOrderTestDep("second-dep", &metMap, t)
	thirdDependency := newRunOrderTestDep("third-dep", &metMap, t).
		HasDependency(&firstDependency).
		HasDependency(&secondDependency)

	fourthDependency := newRunOrderTestDep("fourth-dep", &metMap, t)
	fifthDependency := newRunOrderTestDep("fifth-dep", &metMap, t).
		HasDependency(&thirdDependency).
		HasDependency(&fourthDependency).
		HasDependency(&firstDependency).
		HasDependency(&secondDependency)

	runner := NewDefaultRunner()
	expectedRunOrder := []string{"first-dep", "second-dep", "third-dep", "fourth-dep", "fifth-dep"}

	if err := runner.Run(&fifthDependency); err != nil {
		t.Fatalf("Expected nil runner error, received %s", err)
	}

	if len(runner.meetOrder) != len(expectedRunOrder) {
		t.Fatalf(
			"Expected vs Actual run order length mismatch %d != %d",
			len(expectedRunOrder),
			len(runner.meetOrder),
		)
	}

	for i, expected := range expectedRunOrder {
		if expected != runner.meetOrder[i] {
			t.Fatalf("Run order mismatch at %d, %s != %s", i, expected, runner.meetOrder[i])
		}
	}
}
